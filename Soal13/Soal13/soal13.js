import React from "react"

class IsiTable extends React.Component{
    render() {
        return (
            <div style={{border:'1px solid black', width:'500px',borderRadius:'20px',margin:'20px'}}>
            <img 
                src={this.props.photo}
                alt="new"
                width='500px'
                height='300px'
                style={{borderTopLeftRadius:'20px',borderTopRightRadius:'20px'}}
            />
            
            <div style={{paddingLeft:'20px',marginBottom:'20px'}}>
                <div style={{marginTop:'30px'}}>
                    <b>
                    {this.props.gender==='Male'?'Mr. ':'Mrs. '}
                    {this.props.name}
                    </b>
                </div>

                <div style={{marginTop:'30px'}}>
                    {this.props.profession}
                </div>

                <div style={{marginTop:'30px'}}>
                    {this.props.age} years old
                </div>
            </div>
        </div>
        )

    }
}

class Soal13 extends React.Component{

render() {

    const data = [
        {
            name: "John", 
            age: 25, 
            gender: "Male", 
            profession: "Engineer", 
            photo: "https://media.istockphoto.com/photos/portarit-of-a-handsome-older-man-sitting-on-a-sofa-picture-id1210237745"
        },
        {
            name: "Sarah", 
            age: 22, 
            gender: "Female", 
            profession: "Designer", 
            photo: "https://cdn.pixabay.com/photo/2018/01/15/07/51/woman-3083378_960_720.jpg"
        },    
        {
            name: "David", 
            age: 30, 
            gender: "Male", 
            profession: "Programmer", 
            photo: "https://media.istockphoto.com/photos/handsome-mexican-hipster-man-sending-email-with-laptop-picture-id1182472756"
        }, 
        {
            name: "Kate",
            age: 27, 
            gender: "Female", 
            profession: "Model", 
            photo: "https://cdn.pixabay.com/photo/2015/05/17/20/07/fashion-771505_960_720.jpg" 
        }
    ] 

    return (
        <div style={{display:'flex',flexDirection:'row',flexWrap:'wrap'}}>

        {data.map((item,index)=>{
            return (
                <IsiTable name={item.name} age={item.age} gender={item.gender} profession={item.profession} photo={item.photo}/>   
            )
        })}
    
        </div>
    );
  }

}
export default Soal13