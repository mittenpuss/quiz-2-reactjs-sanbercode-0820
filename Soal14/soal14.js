//soal 14
const volumeBalok=(...args)=>{
    return args.reduce((a,b)=>{
        return a*b
    })
}

const volumeKubus=(...args)=>{
    return args.reduce((a,b)=>{
        return a*b
    })
}

let panjangBalok= 2
let lebarBalok=3
let tinggiBalok=4
let sisiKubus=5

console.log(volumeBalok(panjangBalok,lebarBalok,tinggiBalok))
console.log(volumeKubus(sisiKubus,sisiKubus,sisiKubus))